<!DOCTYPE html>
<html>
	<head>
		<title> Upload.class - Page de test </title>		
		<meta charset="UTF-8">	
					
</head>	
	<body>
		<h1> Fonction upload - Page de test </h1>
		<form id="formu" name="form_post" method="post" action="upload_file.php" enctype="multipart/form-data">	
			<label for="path" class="form-control">Chemin du dossier de destination</label><br />
			<input  type="text" name="path" class="form-control" placeholder="../monChemin/monDossier"/><br /><br />

			<label for="filename" class="form-control">Nom du fichier</label><br />
			<input  type="text" name="filename" class="form-control" placeholder="nomDeMonFichier"/><br /><br />

			<label for="ext" class="form-control">Extentions acceptées (séparer chaque extentions par une virgule ou un espace)</label><br />
			<input  type="text" name="ext" class="form-control" placeholder="jpg, png, doc"/><br /><br />
			
			<label for="width" class="form-control">Largeur désirée (sans le px)</label><br />
			<input  type="text" name="width" class="form-control" placeholder="200"/><br /><br />

			<label for="height" class="form-control">Hauteur désirée (sans le px)</label><br />
			<input  type="text" name="height" class="form-control" placeholder="500"/><br /><br />
			
			<label for="ch_file" class="form-control">Choisissez votre fichier à uploader</label><br />
			<input  type="file" name="ch_file" class="form-control" /><br /><br />

			<input  type="submit"  value="envoyer fichier" class="btn btn-default"/>	
		</form>

	</body>
</html>